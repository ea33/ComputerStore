let elSalaryText = document.getElementById("salaryText")
let elBankBalanceText = document.getElementById("bankBalanceText")
let elLoanText = document.getElementById("loanText")
let elGetLoanBtn = document.getElementById("getLoanBtn")
let elRepayLoanBtn = document.getElementById("repayLoanBtn")
let elLaptopSelection = document.getElementById("laptopSelection")
let elPriceText = document.getElementById("priceText")
let elFeaturesText = document.getElementById("featuresText")
let elComputerImg = document.getElementById("computerImg")
let elSpecsList = document.getElementById("specsList")

let salary = 0
let bankBalance = 0
let currentLoan = 0
let computers = []
let selectedComputerPrice
let selectedComputerName

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(res => res.json())
    .then(data => computers = data)
    .then(computers => addComputersToList(computers))

function addComputersToList(computers){
    computers.forEach(x => addComputerToList(x))
    elPriceText.innerText = `Price: ${computers[0].price} eur`
    elFeaturesText.innerText = computers[0].description
    elComputerImg.src = "https://noroff-komputer-store-api.herokuapp.com/" + computers[0].image
    selectedComputerPrice = computers[0].price
    selectedComputerName = computers[0].title
    computers[0].specs.forEach(x => addSpecToList(x))
}

function addComputerToList(computer){
    const computerElement = document.createElement("option")
    computerElement.value = computer.id
    computerElement.appendChild(document.createTextNode(computer.title))
    elLaptopSelection.appendChild(computerElement)

}

function changeComputer(event){
    const selectedComputer = computers[event.target.selectedIndex]
    elPriceText.innerText = `Price: ${selectedComputer.price} eur`
    elFeaturesText.innerText = selectedComputer.description
    elComputerImg.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image
    selectedComputerPrice = selectedComputer.price
    selectedComputerName = selectedComputer.title
    elSpecsList.innerHTML = ""
    selectedComputer.specs.forEach(x => addSpecToList(x))
}

function addSpecToList(spec){
    const specElement = document.createElement("li")
    specElement.appendChild(document.createTextNode(spec))
    elSpecsList.appendChild(specElement)
}

elLaptopSelection.addEventListener("change", changeComputer)

function addToSalary(){
    salary += 100
    updateSalary()
}

function addToBankBalance(){
    if(salary == 0) return
    //if user has a loan pay 10% of the salary towards loan before bank transfer
    if(currentLoan > 0){
        bankBalance += salary * 0.9
        if(currentLoan < salary * 0.1){
            let bankTransfer = (salary * 0.1) - currentLoan
            currentLoan = 0
            bankBalance += bankTransfer
        } else {
            currentLoan -= salary * 0.1
        }
    //if user hasn't an outstanding loan transfer all money to the bank
    } else {
        bankBalance += salary
    }
    salary = 0
    updateSalary()
    updateBankBalance()
    updateLoan()
}

function openLoanWindow(){
    if(bankBalance <= 0){
        alert("no funds in bank")
    } else {
        takeLoan()
    }
}

function takeLoan(){
    let loan = prompt("Loan amount", "0")
        if (loan > bankBalance*2){
            alert("you don't have enough funds saved for this loan")
        } else if(isNaN(loan)){
            alert("invalid input")
        } else if (currentLoan > 0){
            alert("you must first pay off the previous loan")
        }
        else{
            currentLoan = loan
            bankBalance += currentLoan*1
            updateLoan()
            updateBankBalance()
        }
}

function payOffLoan(){
    if (salary > currentLoan){
        salary -= currentLoan
        currentLoan = 0
    } else {
        currentLoan -= salary
        salary = 0
    }
    updateSalary()
    updateLoan()
}

function buyComputer(){
    if(selectedComputerPrice > bankBalance){
        alert(`your balance is too low to buy the ${selectedComputerName}`)
    } else {
        alert(`You purchased the ${selectedComputerName}`)
        bankBalance -= selectedComputerPrice
        updateBankBalance()
    }
}

function updateSalary(){
    elSalaryText.innerHTML = `Pay: ${salary} eur`
}

function updateBankBalance(){
    elBankBalanceText.innerHTML = `Total balance: ${bankBalance} eur`
}

function updateLoan(){
    elLoanText.innerHTML = `Current loan: ${currentLoan} eur`
    if(currentLoan <= 0){
        //elGetLoanBtn.style.visibility = "visible"
        elGetLoanBtn.disabled = false;
        elGetLoanBtn.style.background = "black"
        elRepayLoanBtn.style.visibility = "hidden"
        elLoanText.style.visibility = "hidden"
    } else {
        //elGetLoanBtn.style.visibility = "hidden"
        elGetLoanBtn.disabled = true;
        elGetLoanBtn.style.background = "gray"
        elRepayLoanBtn.style.visibility = "visible"
        elLoanText.style.visibility = "visible"
    }
}


